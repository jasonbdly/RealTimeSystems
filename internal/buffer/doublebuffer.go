/*
	Author: Jason Bradley
	Class: CS4514, Real Time Systems, Section 01
	Professor: Dr. Edward Jung
	Assignment: Program 1, Collision Detection

	Description:
	Defines the underlying double buffer
	structure used throughout the program.

	Also defines an interface structure that should
	be used to communicate with a double buffer in
	place of direct communication. This interface
	keeps track of which buffer communication
	should be directed to, per each entity
	that interfaces with the double buffer.

	GoLang Specifics:
	"Slice": Go's version of an array. A slice is
		an indirect interface to an underlying array.
		When a slice is allocated, an underlying array
		is created and you instead receive a slice
		that references that array. This design is
		intended to allow efficient manipulation of
		arrays via creating new slices that
		reference the underlying array and almost
		entirely avoiding the need to copy data.
		You could accomplish this in C/C++ as well
		with the correct use of pointers, but this
		behavior is essentially "by default" in Go.
*/

package rtsBuffer

import (
	"errors"
	"sync"
)

//========== DoubleBufferInterface - DEFINITION START ==========

/*
	This structure acts as the primary interface
	between a DoubleBuffer instance, and some
	external entity. It keeps track of the
	next location to read from or write to, and
	increments those cursors each time the
	corresponding operation is performed.

	This logic allows for simultaneous access
	to the same buffer without unnecessary
	blocking of the reader/writer threads.

	A separate DoubleBufferInterface instance
	should be used for each external entity.
*/
type DoubleBufferInterface struct {
	readCursor  int
	writeCursor int
	buffer      *DoubleBuffer
}

func NewDoubleBufferInterface(buffer *DoubleBuffer) *DoubleBufferInterface {
	return &DoubleBufferInterface{
		readCursor:  0,
		writeCursor: 0,
		buffer:      buffer,
	}
}

/*
	Sends a write request to the appropriate
	buffer in the attached double buffer,
	depending on the current cursor position.

	Also increments the write cursor once the
	operation is complete.
*/
func (i *DoubleBufferInterface) Write(data BufferData) error {
	err := i.buffer.WriteTo(i.writeCursor, data)
	if err != nil {
		return err
	}

	i.SetWriteCursor(i.writeCursor + 1)

	return nil
}

/*
	Sends a read request to the appropriate
	buffer in the attached double buffer,
	depending on the current cursor position.

	Also increments the read cursor once the
	operation is complete.
*/
func (i *DoubleBufferInterface) Read() (BufferData, error) {
	data, err := i.buffer.ReadFrom(i.readCursor)
	if err != nil {
		return nil, err
	}

	i.SetReadCursor(i.readCursor + 1)

	return data, nil
}

func (i *DoubleBufferInterface) SetWriteCursor(index int) {
	i.writeCursor = loopedModulus(index, 2)
}

func (i *DoubleBufferInterface) SetReadCursor(index int) {
	i.readCursor = loopedModulus(index, 2)
}

func (i *DoubleBufferInterface) Size() int {
	return i.buffer.Size()
}

//========== DoubleBufferInterface - DEFINITION END ==========

//========== DoubleBuffer - DEFINITION START ==========
// Simply type aliasing to facilitate
// compile-time type validation.
type BufferData []int

/*
	This structure stores a slice for each buffer,
	in addition to an access lock for each buffer.

	Reads/Writes can be directed to either buffer,
	it simply ensures that access to either buffer
	is controlled by a mutex.
*/
type DoubleBuffer struct {
	data      []BufferData
	dataLocks []*sync.Mutex
}

func NewDoubleBuffer(capacity int) *DoubleBuffer {
	// Allocate space for both internal buffers
	data := make([]BufferData, 2, 2)
	data[0] = make(BufferData, capacity, capacity)
	data[1] = make(BufferData, capacity, capacity)

	// Create a mutex for each buffer, to be used
	// for access control
	locks := make([]*sync.Mutex, 2, 2)
	locks[0] = &sync.Mutex{}
	locks[1] = &sync.Mutex{}

	return &DoubleBuffer{
		data:      data,
		dataLocks: locks,
	}
}

/*
	Copies data from the incoming slice into the buffer's
	internal slice. Also ensures that access to that buffer
	is blocked until the write operation is complete.
*/
func (b *DoubleBuffer) WriteTo(index int, data BufferData) error {
	if index < 0 || index >= len(b.data) {
		return errors.New("Buffer index out of range.")
	}

	b.dataLocks[index].Lock()
	defer b.dataLocks[index].Unlock()

	for i := 0; i < len(b.data[index]) && i < len(data); i++ {
		b.data[index][i] = data[i]
	}

	return nil
}

/*
	Copies data from the buffer's internal slice into a new
	slice, and returns that slice. Also ensures that access
	to that buffer is blocked until the read operation is
	complete.
*/
func (b *DoubleBuffer) ReadFrom(index int) (BufferData, error) {
	if index < 0 || index >= len(b.data) {
		return nil, errors.New("Buffer index out of range.")
	}

	b.dataLocks[index].Lock()
	defer b.dataLocks[index].Unlock()

	data := make(BufferData, len(b.data[index]), len(b.data[index]))

	for i := 0; i < len(b.data[index]); i++ {
		data[i] = b.data[index][i]
	}

	return data, nil
}

func (b *DoubleBuffer) Size() int {
	return len(b.data[0])
}

//========== DoubleBuffer - DEFINITION END ==========

//========== UTILITY FUNCTIONS ==========
/*
	Useful utility function to reproduce the
	"looped" behavior of the modulus
	operator in other languages. In Go,
	the modulus operator when used with
	negative numbers evaluates to the
	following expression (Just as in C):
		-a%b = -(a%b)
	As opposed to other languages (Python):
		-a%b = (-a)%b

	This function replicates the behavior
	shown in expression 2.

	This function is primarily used to
	allow infinitely incrementing
	integers within a bounded range
	to "loop" back to the opposite
	end of the range instead of moving
	out of bounds.
*/
func loopedModulus(op1, op2 int) int {
	remainder := op1 % op2
	if (remainder < 0 && op2 > 0) ||
		(remainder > 0 && op2 < 0) {
		return remainder + op2
	}
	return remainder
}
