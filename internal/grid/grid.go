/*
	Author: Jason Bradley
	Class: CS4514, Real Time Systems, Section 01
	Professor: Dr. Edward Jung
	Assignment: Program 1, Collision Detection

	Description:
	Defines "TrackGrid" which is intended to
	abstract the train's and their positions
	away from the underlying layers used for
	each train.
*/

package grid

import (
	"fmt"
)

//========== TrackGrid - DEFINITION START ==========

/*
	Stores the dimensions of the grid, the rune used for
	each train, and references to the layers used for each
	train (indexed by the rune used for that train).
*/
type TrackGrid struct {
	width  int
	height int
	trains []rune
	layers map[rune]*TrackLayer
}

func NewTrackGrid(width, height int) *TrackGrid {
	return &TrackGrid{
		width:  width,
		height: height,
		trains: []rune{},
		layers: make(map[rune]*TrackLayer),
	}
}

/*
	Creates and returns a deep clone of the current
	TrackGrid instance. Will be used as a utility
	to copy the initial train configuration
	to each process in the pipeline.
*/
func (grid *TrackGrid) Clone() *TrackGrid {
	// Copy train runes
	trains := make([]rune, len(grid.trains), len(grid.trains))
	copy(trains, grid.trains)

	// Copy layers for each train
	layers := make(map[rune]*TrackLayer)
	for train, layer := range grid.layers {
		layers[train] = layer.Clone()
	}

	// Return new TrackGrid instance
	return &TrackGrid{
		width:  grid.width,
		height: grid.height,
		trains: trains,
		layers: layers,
	}
}

/*
	Sets up a new train within the grid.
	This includes an initial position, a rune to represent
	the train, and a pointer to a function that defines
	how the train moves.
*/
func (grid *TrackGrid) AddTrain(x, y int, train rune, movementFunc MovementFunction) {
	// Create a new layer for the new train
	layer := NewTrackLayer(grid.width, grid.height, train, movementFunc)

	// Place the train in its initial location
	layer.PlaceTrain(x, y)

	// Track the train and its layer in the grid
	grid.trains = append(grid.trains, train)
	grid.layers[train] = layer
}

/*
	Iterates through each train's layer, and forces
	an update of the train's position, based on its
	movement function.
*/
func (grid *TrackGrid) Update() error {
	for _, layer := range grid.layers {
		_, _, err := layer.Update()
		if err != nil {
			return err
		}
	}

	return nil
}

/*
	Overwrite the grid's entire contents.
	Essentially operates as a "deserialization" function.
*/
func (grid *TrackGrid) SetData(data []int) error {
	layerSize := grid.width * grid.height
	for i, train := range grid.trains {
		startIndex := i * layerSize
		endIndex := startIndex + layerSize
		err := grid.layers[train].SetData(data[startIndex:endIndex])

		// TODO: Implement "rollback" feature for previously updated
		// layers if a subsequent layer fails
		if err != nil {
			return err
		}
	}

	return nil
}

/*
	Gets and returns the data stored in each layer.
	Essentially operates as a "serialization" function.
*/
func (grid *TrackGrid) GetData() []int {
	data := []int{}

	for _, train := range grid.trains {
		layerData := grid.layers[train].GetData()
		data = append(data, layerData...)
	}

	return data
}

/*
	Returns the runes used to represent each train, in
	their originally defined order.
*/
func (grid *TrackGrid) GetTrains() []rune {
	return grid.trains
}

/*
	Uses the layer associated with the requested train
	to determine that train's position.
*/
func (grid *TrackGrid) GetTrainLocation(train rune) (int, int, error) {
	layer := grid.layers[train]

	if layer == nil {
		return -1, -1, fmt.Errorf("Could not find train '%s'.", train)
	}

	x, y, err := layer.GetTrainPosition()
	if err != nil {
		return -1, -1, err
	}

	return x, y, nil
}

/*
	Uses the layers associated with each train
	to determine each train's position. Returns
	their positions in a map, indexed by each train's
	rune.
*/
func (grid *TrackGrid) GetTrainLocations() map[rune][]int {
	trainLocations := make(map[rune][]int)

	for train, layer := range grid.layers {
		trainX, trainY, err := layer.GetTrainPosition()
		if err == nil {
			trainLocations[train] = []int{trainX, trainY}
		}
	}

	return trainLocations
}

/*
	Utility function used to print out the grid's current
	state as a 2D grid.
*/
func (grid *TrackGrid) LayersAsStrings() []string {
	asStrings := []string{}
	for _, layer := range grid.layers {
		asStrings = append(asStrings, layer.String())
	}
	return asStrings
}

//========== TrackGrid - DEFINITION END ==========
