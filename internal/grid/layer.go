/*
	Author: Jason Bradley
	Class: CS4514, Real Time Systems, Section 01
	Professor: Dr. Edward Jung
	Assignment: Program 1, Collision Detection

	Description:
	Defines "TrackLayer" which is intended to
	abstract the majority of the logic used
	to process and manipulate a train within
	the grid space.
*/

package grid

import (
	"errors"
)

//========== TrackLayer - DEFINITION START ==========

/*
	Type alias for a the functions that will be used
	to update the train's position. Provides a bit
	more thorough compile-time checking than simply
	using "func".
*/
type MovementFunction func(x, y int) (int, int)

/*
	Stores the dimensions of the grid, the grid's data,
	the rune used for this train, and the train's movement
	function.
*/
type TrackLayer struct {
	width        int
	height       int
	train        rune
	data         []rune
	movementFunc MovementFunction
}

func NewTrackLayer(width int, height int,
	train rune, movementFunc MovementFunction) *TrackLayer {

	return &TrackLayer{
		width:        width,
		height:       height,
		train:        train,
		data:         make([]rune, width*height, width*height),
		movementFunc: movementFunc,
	}
}

/*
	Creates and returns a deep clone of the current
	TrackLayer instance. Will be used as a utility
	to copy the initial train configuration
	to each process in the pipeline.
*/
func (l *TrackLayer) Clone() *TrackLayer {
	// Copy layer data
	data := make([]rune, len(l.data), len(l.data))
	copy(data, l.data)

	// Return new TrackLayer instance
	return &TrackLayer{
		width:        l.width,
		height:       l.height,
		train:        l.train,
		data:         data,
		movementFunc: l.movementFunc,
	}
}

/*
	Moves the train to the designated location.
	If the train was previously at some other
	location, the rune at that location will
	be reset.
*/
func (l *TrackLayer) PlaceTrain(x, y int) {
	// Scan the grid for the train's current position
	lastX, lastY, err := l.GetTrainPosition()

	// Reset the rune at the train's last position
	if err == nil {
		lastDataPosition := lastY*l.width + lastX
		l.data[lastDataPosition] = 0
	}

	// Place the train's rune at the new position
	dataPosition := y*l.width + x
	l.data[dataPosition] = l.train
}

/*
	Determines the train's next position using its
	movement function, and moves the train to that
	location.
*/
func (l *TrackLayer) Update() (int, int, error) {
	// Scan the grid fro the train's current position
	lastX, lastY, err := l.GetTrainPosition()

	if err != nil {
		return -1, -1, err
	}

	// Determine the train's next position
	nextX, nextY := l.movementFunc(lastX, lastY)

	// "Loop" back to the opposite side if the train
	// goes out of bounds.
	nextX = boundedModulus(nextX, l.width)
	nextY = boundedModulus(nextY, l.height)

	// Move the train to the new location
	l.PlaceTrain(nextX, nextY)

	// Return the new location, to avoid a rescan
	return nextX, nextY, nil
}

/*
	Scans the grid for the train's current position.
	If the train cannot be found, returns -1 for x and y,
	in addition to an error.
*/
func (l *TrackLayer) GetTrainPosition() (int, int, error) {
	dataPosition := -1

	// Scan through the grid's data for the train
	for i := 0; i < len(l.data); i++ {
		if l.data[i] == l.train {
			dataPosition = i
			break
		}
	}

	if dataPosition > -1 {
		// Convert the data position to a grid position
		x := dataPosition % l.width
		y := dataPosition / l.width

		return x, y, nil
	} else {
		return -1, -1,
			errors.New("Could not find the train's position.")
	}
}

/*
	Replaces the grid's data with the supplied data.
	Essentially used as a "deserialize" function.
*/
func (l *TrackLayer) SetData(data []int) error {
	if len(data) != l.width*l.height {
		return errors.New("New data dimensions do not match expected dimensions.")
	}

	// Copying the values over to ensure
	// the original data is left alone
	for i := 0; i < len(data); i++ {
		l.data[i] = rune(data[i])
	}

	return nil
}

/*
	Returns the grid's data.
	Essentially used as a "serialize" function.
*/
func (l *TrackLayer) GetData() []int {
	data := make([]int, len(l.data), len(l.data))

	// Copying the values over to ensure
	// the original data is left alone
	for i := 0; i < len(data); i++ {
		data[i] = int(l.data[i])
	}

	return data
}

/*
	Override standard String method.
	Used to provide a human-interpretable version
	of the grid's data.
*/
func (l TrackLayer) String() string {
	asString := ""
	for i := 0; i < len(l.data); i++ {
		x := i % l.width

		if l.data[i] == l.train {
			asString += string(l.train)
		} else {
			asString += "_"
		}

		if x == l.width-1 {
			asString += "\n"
		}
	}
	return asString
}

//========== UTILITY FUNCTIONS ==========
/*
	Useful utility function to reproduce the
	"looped" behavior of the modulus
	operator in other languages. In Go,
	the modulus operator when used with
	negative numbers evaluates to the
	following expression (Just as in C):
		-a%b = -(a%b)
	As opposed to other languages (Python):
		-a%b = (-a)%b

	This function replicates the behavior
	shown in expression 2.

	This function is primarily used to
	allow infinitely incrementing
	integers within a bounded range
	to "loop" back to the opposite
	end of the range instead of moving
	out of bounds.
*/
func boundedModulus(op1, op2 int) int {
	remainder := op1 % op2
	if (remainder < 0 && op2 > 0) ||
		(remainder > 0 && op2 < 0) {
		return remainder + op2
	}
	return remainder
}

//========== TrackLayer - DEFINITION END ==========
