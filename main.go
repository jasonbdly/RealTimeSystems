/*
	Author: Jason Bradley
	Class: CS4514, Real Time Systems, Section 01
	Professor: Dr. Edward Jung
	Assignment: Program 1, Collision Detection

	GoLang Specifics:
	"rune": Golang's version of Java's char.
		Just a type-alias for int32, with some added
		functions for rune processing.
*/

package main

import (
	"./internal/buffer"
	"./internal/grid"
	"fmt"
	"sync"
	"time"
)

/*
	Simple wrapping structure around the configuration
	needed for a train. Stores its initial position,
	rune, and movement function.
*/
type trainConfig struct {
	x, y         int
	train        rune
	movementFunc func(int, int) (int, int)
}

// ENTRY POINT
func main() {
	// Grid configuration
	trackWidth := 7
	trackHeight := 8
	trackGrid := grid.NewTrackGrid(trackWidth, trackHeight)

	// Set up train initial positions, symbols, and movement functions
	trainConfigs := []trainConfig{
		trainConfig{0, 0, 'X', func(x, y int) (int, int) {
			return x + 1, y + 1
		}},
		trainConfig{2, 0, 'Y', func(x, y int) (int, int) {
			return x, y + 1
		}},
		trainConfig{6, 3, 'Z', func(x, y int) (int, int) {
			return x + 1, y
		}},
	}

	// Add trains to grid
	for _, trainConf := range trainConfigs {
		trackGrid.AddTrain(
			trainConf.x, trainConf.y,
			trainConf.train,
			trainConf.movementFunc,
		)
	}

	// A/B - Buffer capacity = N_LAYERS * WIDTH * HEIGHT
	trackGridBuffer := rtsBuffer.NewDoubleBuffer(
		len(trainConfigs) * trackWidth * trackHeight)

	// C/D - Buffer capacity = N_LAYERS * 3 (Train Rune,
	// X Position, Y Position)
	trainPositionBuffer := rtsBuffer.NewDoubleBuffer(
		len(trainConfigs) * 3)

	// Initialize buffer "A" slot with current grid data
	trackGridBuffer.WriteTo(0, trackGrid.GetData())

	// Time tracking - For stopping execution of all
	// processes, and general time tracking
	processTimeStarted := time.Now()
	processUpdateInterval := time.Second
	timeToStop := processTimeStarted.Add(20 * time.Second)

	// Wait group to synchronize main goroutine with all
	// sub-goroutines
	var processWaitGroup sync.WaitGroup
	processWaitGroup.Add(3)

	// Start process 1 (Update train positions within
	// track grid, A<->B)
	go func() {
		// Signal process 1 complete, once this function
		// returns
		defer processWaitGroup.Done()

		// Stores the first error encountered during
		// process execution
		var procErr error

		// Create a new interface to the
		// track grid buffer (A/B) for this process
		buffer := rtsBuffer.NewDoubleBufferInterface(trackGridBuffer)

		// Clone a TrackGrid instance for this process
		gridInterface := trackGrid.Clone()

		// Increment write cursor to account for buffer
		// "A" initialization above
		buffer.SetWriteCursor(1)

		// Start process 1 loop
		for time.Now().Before(timeToStop) {
			// Read track data from the last execution
			lastTrackData, err := buffer.Read()
			if err != nil {
				procErr = err
				break
			}

			// Update the grid interpreter with
			// the last known data
			gridInterface.SetData(lastTrackData)

			// Have the grid interpreter update
			// the position of all trains
			gridInterface.Update()

			// Retrieve the grid's new data
			newTrackData := gridInterface.GetData()

			// Write the updated grid data
			// back into the A/B double buffer
			err = buffer.Write(newTrackData)
			if err != nil {
				procErr = err
				break
			}

			// Sleep until ready for the next execution
			time.Sleep(processUpdateInterval)
		}

		// Print out any errors
		if procErr != nil {
			fmt.Println("Process 1 Failed: " + procErr.Error())
		}
	}()

	// Start process 2 (Calculate train positions)
	go func() {
		// Signal process 2 complete, once this function
		// returns
		defer processWaitGroup.Done()

		// Stores the first error encountered during
		// process execution
		var procErr error

		// Create a new interface to the
		// track grid buffer (A/B) for this process
		readBuffer := rtsBuffer.NewDoubleBufferInterface(trackGridBuffer)

		// Create a new interface to the
		// train position buffer (C/D) for this process
		writeBuffer := rtsBuffer.NewDoubleBufferInterface(trainPositionBuffer)
		gridInterface := trackGrid.Clone()

		// Start process loop
		for time.Now().Before(timeToStop) {
			// Read track data from last execution
			trackData, err := readBuffer.Read()
			if err != nil {
				procErr = err
				break
			}

			// Update grid with last known track data
			gridInterface.SetData(trackData)

			// Retrieve all train locations from grid
			trainLocationData := make([]int, writeBuffer.Size(), writeBuffer.Size())
			trainLocations := gridInterface.GetTrainLocations()

			// Process train locations and format as appropriate to be placed in buffer C/D
			for i, train := range gridInterface.GetTrains() {
				location := trainLocations[train]
				trainLocationData[i*3] = int(train)
				trainLocationData[i*3+1] = location[0]
				trainLocationData[i*3+2] = location[1]
			}

			// Write the train locations to buffer C/D
			err = writeBuffer.Write(trainLocationData)
			if err != nil {
				procErr = err
				break
			}

			// Sleep until ready for the next execution
			time.Sleep(processUpdateInterval)
		}

		// Print out any errors
		if procErr != nil {
			fmt.Println("Process 2 Failed: " + procErr.Error())
		}
	}()

	// Start process 3 (Process train positions and notify of collisions)
	go func() {
		// Signal process 3 complete, once this function
		// returns
		defer processWaitGroup.Done()

		collisionFoundMsg :=
			"Collision between %s and %s at second %d, location (%d, %d).\n"

		noCollisionFoundMsg :=
			"No collision detected at second %d.\n"

		// Sleep for one update interval to ensure process 2 completes first execution
		time.Sleep(processUpdateInterval)

		// Stores the first error encountered during
		// process execution
		var procErr error

		// Create a new interface to the
		// train position buffer (C/D) for this process
		readBuffer := rtsBuffer.NewDoubleBufferInterface(trainPositionBuffer)

		// Clone a TrackGrid instance for this process
		gridInterface := trackGrid.Clone()

		// Start process loop
		for time.Now().Before(timeToStop) {
			// Read train position data from C/D
			trainPositionData, err := readBuffer.Read()
			if err != nil {
				procErr = err
				break
			}

			// Organize each position by train to make
			// future processing simpler
			positionsByTrain := make(map[rune][]int)
			trainsInGrid := gridInterface.GetTrains()
			for i := 0; i < len(trainsInGrid); i++ {
				trainRune := rune(trainPositionData[i*3])
				trainXPosition := trainPositionData[i*3+1]
				trainYPosition := trainPositionData[i*3+2]
				positionsByTrain[trainRune] =
					[]int{trainXPosition, trainYPosition}
			}

			// Check all combinations of each train for
			// collisions
			collisionDetected := false
			for i, trainA := range trainsInGrid {
				trainAPosition := positionsByTrain[trainA]

				// Check train A against each other train
				for j := i + 1; j < len(trainsInGrid); j++ {
					trainB := trainsInGrid[j]
					trainBPosition := positionsByTrain[trainB]

					// Check if positions are the same
					areColliding := trainAPosition[0] == trainBPosition[0] &&
						trainAPosition[1] == trainBPosition[1]

					// Print collision message to console
					// if error detected
					if areColliding {
						collisionDetected = true
						fmt.Printf(
							collisionFoundMsg,
							string(trainA), string(trainB),
							time.Since(processTimeStarted).Round(time.Second)/time.Second,
							trainAPosition[0], trainAPosition[1])
					}
				}
			}

			// If no collision found this iteration, notify
			// in console
			if !collisionDetected {
				fmt.Printf(noCollisionFoundMsg,
					time.Since(processTimeStarted).Round(time.Second)/time.Second)
			}

			// Sleep until ready for the next execution
			time.Sleep(processUpdateInterval)
		}

		// Print out any errors
		if procErr != nil {
			fmt.Println("Process 3 Failed: " + procErr.Error())
		}
	}()

	// Wait for all processes to signal completion before
	// allowing the main process to exit
	processWaitGroup.Wait()
	fmt.Println("All processes complete")
}
